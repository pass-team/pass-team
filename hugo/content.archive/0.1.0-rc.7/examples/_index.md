---
lastmod: 2022-12-13
publishdate: 2022-11-15
title: Examples
summary: Usage examples for `pass team` from a fresh setup to a populated password store with shared and personal passwords.
---
