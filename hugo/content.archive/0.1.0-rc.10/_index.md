---
lastmod: 2022-12-13
title: 'Release Candidate 0.1.0-rc.10'
publishdate: '2022-12-06'
summary: 'The version 0.1.0-rc.10 is a release candidate. New features will not be implemented until the up-coming version 0.1.0. Bug fixes and updates to the documentation may still be included into the target version.'
keywords: 'Pass, Team, Password Store, Share, Release, 0.1.0-rc.10, Release Candidate 0.1.0-rc.10'
---
