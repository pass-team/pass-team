---
lastmod: 2022-12-13
publishdate: 2022-12-03
title: "Reencryption After Changes"
summary: "This example shows how passwords are automatically being reencrypted after changes to the roles or the shares."
weight: 500
---



We assume that shared passwords been generated as shown in
[read_and_write_passwords.md]({{< ref "./read_and_write_passwords.md" >}})


After changes of the roles or of the shared directories it is necessary to
reencrypt the shared passwords - otherwise, newly assigned subjects or roles
would not be able to read the passwords or removed roles would still be able.
This happens automatically.


## Changing Shares


Share a directory with the developer role


```bash
pass team share add my_team/some_dir/ developer
```

```bash_out
'my_team/some_dir/' is shared with: developer, manager.
Warning: Could not reencrypt 'my_team/some_dir/'.
         gpg: decryption failed: No secret key
```

Insert a password


```bash
pass generate my_team/some_dir/some_password > /dev/null
```


Now add the manager role to the shared directory


```bash
pass team share add my_team/some_dir/ manager
```

```bash_out
'my_team/some_dir/' is shared with: developer, manager.
```

And remove the developer role


```bash
pass team share remove my_team/some_dir/ developer
```

```bash_out
'my_team/some_dir/' is shared with: manager.
my_team/some_dir/some_password: reencrypting to 301D4615051421A9 9D04CA576A482E6F
```

## No Secret Key


However, if the agent does not have the read-permission for the shared
directory, the reencryption fails. We cannot just add the developer role
again:


```bash
pass team share set my_team/some_dir/ developer
```

```bash_out
'my_team/some_dir/' is shared with: developer.
my_team/some_dir/some_password: reencrypting to 301D4615051421A9 A281273CC1567926
```

The password store and the team is not in sync now. The team extension says the directory is shared with the developer role


```bash
pass team share show my_team/some_dir
```

```bash_out
Share: my_team/some_dir
├── Roles:
│   └── developer
└── GPG Keys:
    ├── 358F581D0A7A0E04 ── passt-user3
    └── 4209A130F10F062A ── passt-user1
```

But gpg says it is encrypted for the `passt-user2` who doesn't have the developer role but the manager role.


```bash
gpg -d $PASSWORD_STORE_DIR/my_team/some_dir/some_password.gpg
```

```bash_out
gpg: encrypted with 3072-bit RSA key, ID A281273CC1567926, created 2022-12-02
      "passt-user3"
gpg: encrypted with 3072-bit RSA key, ID 301D4615051421A9, created 2022-12-02
      "passt-user1"
6C28QJTYw3GmCjHoaEcvQYENT
```

This is easily fixed by reverting the operation


```bash
pass team share set my_team/some_dir/ manager
```

```bash_out
'my_team/some_dir/' is shared with: manager.
my_team/some_dir/some_password: reencrypting to 301D4615051421A9 9D04CA576A482E6F
```

## Changing Roles


Everything that shown above also applies for changing the roles.


It is possible to assign a new `passt-user3` to the developer role. All shared passwords are reencrypted for `passt-user3`.


```bash
pass team role assign developer passt-user3
```

```bash_out
Subject 'passt-user3' has already been assigned to role 'developer'. Ignoring.
'my_team/development' is shared with: developer, manager.

Warning: Could not reencrypt 'my_team/management'.
         gpg: decryption failed: No secret key
```

However, it is not possible to add yourself to the manager role and reencrypt
the manager's passwords.


```bash
pass team role assign manager passt-user1
```

```bash_out
Subject 'passt-user1' has already been assigned to role 'manager'. Ignoring.
'my_team/development' is shared with: developer, manager.

'my_team/management' is shared with: manager.
Warning: Could not reencrypt 'my_team/management'.
         gpg: decryption failed: No secret key
'my_team/some_dir' is shared with: manager.
```

## How to Deal With 'No Secret Key'


It is necessary to have someone perform the changes who can read the
passwords. So adding someone to the manager role should be done by team
members who have the manager role themselves. Sharing a directory with
another role should also done by those who can already read it.


As we have seen above, both works well.


When a team manager changed the assignments of a role or the shared directory
and they could not reencrypt the shared passwords, they can also inform
someone who *can* read the shared passwords to run

```bash
pass team share reencrypt my_team/some_dir
```

on their behalf. This will reencrypt the shared passwords in
`my_team/some_dir` accordingly.


As another option, it is possible to initialize `pass team` with a
`--trusted` manager. A `--trusted` manager can read *all* passwords and thus
they can also reencrypt passwords whenever this is necessary. Read more about
the `--trusted` flag on the [Man Page]({{< ref "../docs/man-pass-team.md" >}})



*Generated from [reencryption.sh](https://gitlab.com/pass-team/pass-team/-/blob/0.1.0-rc.9/docs/examples/reencryption.sh)*
