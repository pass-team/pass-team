---
lastmod: 2022-12-13
publishdate: 2022-05-31
title: Examples
summary: Usage examples for `pass team` from a fresh setup to a populated password store with shared and personal passwords.
---
