---
title: Concepts
summary: Central concepts of Role-based Access Management and how they are implemented in `pass team`.
sitemap:
 priority: 0.5
---
