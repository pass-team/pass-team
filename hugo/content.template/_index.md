---
title: "Pass Team"
sitemap:
 priority: 0.9
---
Share your passwords with your team.  
`pass team` is an extension for [pass - the standard unix password manager](https://www.passwordstore.org/).
