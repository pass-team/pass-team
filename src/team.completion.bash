__pass_team_list_pub_key_uids () {
    readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "$(gpg -k --with-colons | awk -F: '$1=="uid" { print $10 }')" -- "${1}")
}

__pass_team_list_roles () {
    readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "$(pass team -q role list 2>/dev/null)" -- "${1}")
}

__pass_team_role_complete () {
    COMPREPLY=()

    local cur="${COMP_WORDS[COMP_CWORD]}"
    if [[ $COMP_CWORD -eq 3 ]]; then
        readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "list show create remove assign unassign --help" -- "${cur}")
    elif [[ $COMP_CWORD -eq 4 ]]; then

        case "${COMP_WORDS[3]}" in
            show|rm|remove|delete|assign|unassign)
                __pass_team_list_roles "${cur}"
                ;;
        esac
    elif [[ $COMP_CWORD -gt 4 ]]; then

        case "${COMP_WORDS[3]}" in
            assign)
                # TODO Remove all assigned keys from the output
                __pass_team_list_pub_key_uids "${cur}"
                ;;
            unassign)
                # TODO all gpg-id currently assigned to "$role"
                # + --all
                #local role="${COMP_WORDS[4]}"
                #COMPREPLY=($(compgen -W "current gpg-ids of $role" -- ${cur}))
                ;;
        esac
    fi
}

__pass_team_list_shares () {
    readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "$(pass team -q share list 2>/dev/null)" -- "${1}")
}

__pass_team_share_complete () {
    COMPREPLY=()

    local cur="${COMP_WORDS[COMP_CWORD]}"
    if [[ $COMP_CWORD -eq 3 ]]; then
        readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "list show add set reencrypt remove unset --help" -- "${cur}")
    elif [[ $COMP_CWORD -eq 4 ]]; then

        case "${COMP_WORDS[3]}" in
            show|rm|remove|unset)
                __pass_team_list_shares "${cur}"
                ;;
            add|set|reencrypt)
                _pass_complete_folders
                ;;
        esac
    elif [[ $COMP_CWORD -gt 4 ]]; then

        case "${COMP_WORDS[3]}" in
            rm|remove|add|set)
                ## TODO remove assigned/not assigned roles
                __pass_team_list_roles "${cur}"
                ;;
        esac
    fi
}

__pass_team_init_complete () {
    COMPREPLY=()

    local cur="${COMP_WORDS[COMP_CWORD]}"

    case "${COMP_WORDS[$COMP_CWORD-1]}" in
        --trusted)
            __pass_team_list_pub_key_uids "${cur}"
            ;;
        *)
            readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "--trusted" -- "${cur}")
            __pass_team_list_pub_key_uids "${cur}"
            ;;
    esac
}

__pass_team_help_complete () {
    COMPREPLY=()

    local cur="${COMP_WORDS[COMP_CWORD]}"
    local commands="role share"
    readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "${commands}" -- "${cur}")
}

__password_store_extension_complete_team() {
    COMPREPLY=()
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local commands="init role share --version --help --debug --license --no-color --quiet"
    if [[ $COMP_CWORD -gt 2 ]]; then
        case "${COMP_WORDS[2]}" in
            role)
                __pass_team_role_complete
                ;;
            share)
                __pass_team_share_complete
                ;;
            init)
                __pass_team_init_complete
                ;;
            help|--help|-h)
                __pass_team_help_complete
        esac
    else
        readarray -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "${commands}" -- "${cur}")
    fi
}

PASSWORD_STORE_EXTENSION_COMMANDS+=(team)
export PASSWORD_STORE_EXTENSION_COMMANDS
