# typed: false
# frozen_string_literal: false

# Pass Team is an extension for Pass.
class PassTeam < Formula
  version ""
  desc "This is a team extension for pass - the standard unix password manager"
  homepage "https://gitlab.com/pass-team/pass-team/"
  head "https://gitlab.com/pass-team/pass-team", branch: "main"
  url ""
  sha256 ""
  license "GPL-3.0-or-later"

  depends_on "pass"

  def install
    system "make", "PREFIX=#{prefix}",
           "PASS_EXTENSION_DIR=\"#{prefix}/lib/password-store/extensions\"",
           "WITH_BASHCOMP=yes",
           "WITH_MAN=yes",
           "mandir=\"#{man}\"",
           "BASHCOMPDIR=\"#{bash_completion}\"",
           "-j1", "install"
  end

  test do
    system "make", "test"
  end
end
