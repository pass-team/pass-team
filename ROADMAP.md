# Roadmap

This roadmap is a living document and outlines the next steps towards the
[Goals](./VISION_AND_GOALS.md) of this project.

## 0.2 -- Refactoring and Minimal Key Management

* Add `pass team key [list|show]` commands for listing/showing keys used in the
  password store, prettily formatted.
* Refactor `pass team share` to account for password stores which are not
  initializes freshly but contain passwords and use different keys already.

## 0.3 -- Use 'pass git' for cooperation and backups of pass-team data

* Add convenient .gitignore and config settings for sharing pass team data over
  git repositories.
* Automatically add pass-team's files to the git repo.

## 0.4 -- Check/verify the security and consistency of the pass-team data

* Sign all relevant files by the team managers' gpg-keys
* Check signature of the relevant files and warn about
  expired/untrusted/missing/broken signatures.

## 0.x -- Key-Management

* Manage and distribute the public gpg keys of the team members via pass team
* Augment the `pass team key` commmand (add/remove/update).
* Add convenience method for importing/exporting key to/from gpg-keystore.

## 0.x -- Passwords in need for an update

* When reencryption of passwords fails, because the pass team manager doesn't
  have reading permissions, privileged users need to reencrypt the password
  after the or roles/shares have changed. Implement a special "reencryption
  needed" tag.
* When team members lose the read permission for a password, the passwords are
  marked as "compromised".
* List "compromised" and "reencryption needed" passwords.
* "compromised" tag is automatically removed when a password is being
  changed.
* "reencryption needed" tag is being removed when the password is being
  reencrypted.
* Also, when an evil user creates a password for their boss, and tells them "Oh, I created a social media account for you and set everything up because I know that you are soooo busy...." The boss muss must remember to reset the password because otherwise, the evil user might just remember the password even it is not readable anymore. Same idea here, just mark the password as "compromised".

## 0.x -- Multiple Teams

* Allow multiple teams in a single password store.

# Milestones From the Past

## 0.1.0 (2023-04-16) -- Basic Role-based Access Control

* Initialize a team with team managers
  `pass team init <gpg-id>...`
  * normal team managers can manage roles and shares.
  * trusted team managers additionally have read-access to all passwords.
* Manage roles (list/add/remove/assign/unassign).
  `pass team role ...`
* Manage shares (list/add/remove)
  `pass team share`
* Automatically reencrypt passwords after roles or shares have been changed.
* Verbosity levels for debugging.
  `pass team -d[d]+ ...`
* Quiet option for more machine-readable output
  `pass team -q ...`
* Help
  `pass team --help`
