# Examples

Usage examples for `pass team` from a fresh setup to a populated password store with shared and personal passwords.


## Fresh Setup

[fresh_setup.sh](./fresh_setup.sh) - Example for a fresh setup of `pass team`.


## Roles Management

[roles_management.sh](./roles_management.sh) - Examples for listing, adding, viewing, assigning, unassigning and removing roles. This example is build upon the [Fresh Setup](./fresh_setup.sh) example.


## Shared Password Directories

[shared_password_directories.sh](./shared_password_directories.sh) - Example for sharing and unsharing directories, assigning roles to the shared directories and inspecting and listing shared directories. This example is build upon the [Roles Management](./roles_management.sh) example.


## Read and Write Passwords

[read_and_write_passwords.sh](./read_and_write_passwords.sh) - Example for reading and writing passwords in the shared directories. This example is build upon the [Shared Password Directories](./shared_password_directories.sh) example.


## Reencryption After Changes

This example shows how passwords are automatically being reencrypted after changes to the roles or the shares. This example is build upon the [Read and Write Passwords](./read_and_write_passwords.sh) example.
