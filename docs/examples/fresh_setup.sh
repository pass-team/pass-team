#!/bin/bash

# # Fresh Setup

# We assume that the gpg key store is ready and contains private keys for:
# * passt-manager1
# * passt-user1
#
# as well as a public key for:
# * passt-user2

# Initialize a fresh password store:
pass init passt-user1

# Initialize a fresh team:
pass team init passt-manager1
