#!/bin/bash

# # Shared Password Directories

# We assume that roles have been created as shown in
# ./roles_management.sh


# Create a shared directory for different purposes:
pass team share add my_team/development developer manager

pass team share add my_team/management developer manager


# Using `set` instead of `add` removes all roles from the given share and adds
# only the new ones.
pass team share set my_team/management manager


# List all shares:
pass team share list


# Show a particular share:
pass team share show my_team/management


# Use `remove` to remove a particular role:
pass team share add my_team/developer-secrets developer manager

pass team share remove my_team/developer-secrets manager # ;)


# When `remove` removes the last role from a share, the directory is not shared anymore.
pass team share remove my_team/developer-secrets developer


# Use `unset` to remove a share entirely. This is equivalent to
# `share remove <directory> --all`.
pass team share add some/wrong/path developer

pass team share unset some/wrong/path


# Error handling:
pass team share show some/wrong/path

pass team share unset some/wrong/path
