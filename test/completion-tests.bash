# shellcheck disable=SC1091
source /usr/share/bash-completion/completions/pass
source ./src/team.completion.bash

source ./test/utils.bash
# shellcheck disable=SC2034
PASS_TEAM_UNIT_TESTS=ENABLED
export PASSWORD_STORE_TEAM_QUIET=
# shellcheck disable=SC1090
source "$FILE_UNDER_TEST"
set +o nounset # dont exit when undeclared variables are being used
set +o errexit # dont exit when a command fails

export PREFIX="${TEST_DIR:-./dev-password-store}"

oneTimeTearDown () {
    [[ -d "$TEST_DIR" ]] && rm -r "$TEST_DIR"
}

oneTimeSetUp () {
    pass init passt-user1
    pass team init passt-manager1
}

getCompletion () {
    COMP_WORDS=("pass" "team")
    [ -n "$*" ] && COMP_WORDS+=("$@")
    COMP_CWORD="${#COMP_WORDS[@]}"
    _pass
    echo "${COMPREPLY[*]}"
}

testPassTeam () {
    assertEquals "init role share --version --help --debug --license --no-color --quiet" "$(getCompletion)"
    assertEquals "role share" "$(getCompletion "--help")"
}

testPassTeamInit () {
    assertEquals "--trusted passt-manager1 passt-manager2 passt-user1 passt-user2 passt-user3 passt-user4 passt-user5" "$(getCompletion "init")"

    assertEquals "passt-manager1 passt-manager2 passt-user1 passt-user2 passt-user3 passt-user4 passt-user5" "$(getCompletion "init" "--trusted")"
}

testPassTeamRole () {
    assertEquals "list show create remove assign unassign --help" "$(getCompletion "role")"
    assertEquals "help" "" "$(getCompletion "role" "--help")"
    assertEquals "show" "" "$(getCompletion "role" "--show")"

    # add a role
    cmd_role_create "test_role"
    assertEquals "show 2" "test_role" "$(getCompletion "role" "show")"

    # add a role
    cmd_role_create "test_role2"
    assertEquals "show 3" "test_role test_role2" "$(getCompletion "role" "show")"

    assertEquals "remove" "test_role test_role2" "$(getCompletion "role" "remove")"
    assertEquals "assign" "test_role test_role2" "$(getCompletion "role" "assign")"
    assertEquals "assign 2" "passt-manager1 passt-manager2 passt-user1 passt-user2 passt-user3 passt-user4 passt-user5" "$(getCompletion "role" "assign" "test_role")"
    # assign passt-user1
    cmd_role_assign "test_role2" "passt-user5"
    # TODO assertEquals "assign 3" "passt-manager1 passt-manager2 passt-user1 passt-user2 passt-user3 passt-user4" "$(getCompletion "role" "assign" "test_role")"
    assertEquals "unassign" "test_role test_role2" "$(getCompletion "role" "unassign")"
    # TODO assertEquals "unassign" "passt-user5" "$(getCompletion "role" "unassign" "test_role")"

    assertEquals "nonsense" "" "$(getCompletion "role" "bla")"
}

testPassTeamShare () {
    assertEquals "list show add set reencrypt remove unset --help" "$(getCompletion "share")"
    assertEquals "help" "" "$(getCompletion "share" "--help")"
    assertEquals "show" "" "$(getCompletion "share" "show")"
    # add a share
    pass team share add "test_share/" "test_role"
    assertEquals "show" "test_share/" "$(getCompletion "share" "show")"
    # add a share
    pass team share add "test_share2/" "test_role2"
    assertEquals "show" "test_share/ test_share2/" "$(getCompletion "share" "show")"

    assertEquals "remove" "test_share/ test_share2/" "$(getCompletion "share" "remove")"
    assertSetEquals "set" "test_share/ test_share2/" "$(getCompletion "share" "set")"
    assertEquals "unset" "test_share/ test_share2/" "$(getCompletion "share" "unset")"
    assertEquals "set 2" "test_role test_role2" "$(getCompletion "share" "set" "test_share/")"


    assertSetEquals "add" "test_share/ test_share2/" "$(getCompletion "share" "add")"
    # TODO assertEquals "add 2" "test_role2" "$(getCompletion "share" "add" "test_share")"
    # TODO assertEquals "remove 2" "test_role" "$(getCompletion "share" "remove" "test_share")"
}

# shellcheck disable=SC1091
source shunit2
