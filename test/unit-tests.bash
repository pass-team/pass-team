#!/bin/bash

# shellcheck disable=SC2317
source ./test/utils.bash
# shellcheck disable=SC2034
PASS_TEAM_UNIT_TESTS=ENABLED
FILE_UNDER_TEST=${FILE_UNDER_TEST:-./src/team.bash}
# shellcheck disable=SC1090
source "$FILE_UNDER_TEST"
set +o errexit

# some vars to mock pass
TEST_DIR=".unit-tests-dir"
PREFIX="${TEST_DIR}/.test-password-store"
SHARED_HINT_FILE=${SHARED_HINT_FILE-.team.shared}

setUp () {
    tearDown
    mkdir "$TEST_DIR"
}

tearDown () {
    [[ -d "$TEST_DIR" ]] && rm -r "$TEST_DIR"
    set_quiet
}

oneTimeTearDown () {
    tearDown
}

testExpectIsKnownPubkey () {
    is_known_public_key "" && fail 'is_known_public_key "" shoud return false'
    is_known_public_key "basdfjasdjfh" && fail 'is_known_public_key "basdfjasdjfh" should return false'
    return 0
}

setupIntializedPassTeam() {
    # mock intialized pass and pass-team
    mkdir -p "$PREFIX/$ROLE_DIR"
    local gpgid
    gpgid="gpg_1_$(randStr 8)"
    echo "$gpgid" > "$PREFIX/$PASSWORD_STORE_TEAM_DIR/.gpg-id"
}

testCheckInitialed () {
    assertFalse "pass team should be uninitialized." is_initialized
    setupIntializedPassTeam
    assertTrue "pass team should be initialized." is_initialized

    # now run actual check_initialized as well
    expect_initialized
}

testCmdTeamInit () {
    PASS_TEAM_QUIET=0

    # mock gpg
    gpg () {
        local params=("$@")
        echo "GPG(mock-up) ${params[*]}"
    }

    local gpgid=("gpg_1_$(randStr 8)" "gpg_2_$(randStr 8)")

    # mock is_pass_initialized
    is_pass_initialized () {
        return 0
    }
    # mock get_fingerprint
    get_fingerprint () {
        echo "$1"
    }
    # mock pass cmd_init
    cmd_init () {
        assertEquals "--path=$PASSWORD_STORE_TEAM_DIR" "$1"
        assertEquals "${gpgid[0]}" "$2"
        mkdir -p "$PREFIX/$PASSWORD_STORE_TEAM_DIR"
    }

    local out
    # exit with usage hint
    out="$(cmd_team_init 2>&1 || true)"
    assertTrue "usage hint 1" "echo \"${out}\" | grep -c \"Usage:\""

    # exit with usage hint
    out="$(cmd_team_init - 2>&1 || true)"
    assertTrue "usage hint 2" "echo \"${out}\" | grep -c \"Usage:\""

    # exit with usage hint
    out="$(cmd_team_init -- 2>&1 || true)"
    assertTrue "usage hint 3" "echo \"${out}\" | grep -c \"Usage:\""

    # show help and exit
    out="$(cmd_team_init --help)"
    assertTrue "help 1" "echo \"${out}\" | grep -c \"Commands:\""

    # show help and exit
    out="$(cmd_team_init -h)"
    assertTrue "help 2" "echo \"${out}\" | grep -c \"Commands:\""

    cmd_team_init "${gpgid[@]}"
    assertDirExists "$PREFIX/$ROLE_DIR"
    assertFileNotExists "$PREFIX/$PASSWORD_STORE_TEAM_DIR/.trusted-gpg-id"

    cmd_team_init --trusted "${gpgid[0]}" "${gpgid[1]}"
    assertFileLinesEquals "$PREFIX/$PASSWORD_STORE_TEAM_DIR/.trusted-gpg-id" "${gpgid[0]}"

    cmd_team_init "${gpgid[0]}" --trusted "${gpgid[1]}"
    assertFileLinesEquals "$PREFIX/$PASSWORD_STORE_TEAM_DIR/.trusted-gpg-id" "${gpgid[1]}"
}

setupRoleExists() {
    setupIntializedPassTeam
    cmd_role_create "$1"
}

testCmdRoleInsert () {
    local role
    role="testrole_1_$(randStr 8)"
    local role_file="$PREFIX/$ROLE_DIR/$role"
    setupIntializedPassTeam

    cmd_role_create "$role"
    assertFileExists "$role_file"
    assertFileEmpty "$role_file"
}

testCmdRoleAssign () {
    die () {
        echo "ERROR: $1"
    }
    local role gpgid role_file
    role="testrole_1_$(randStr 8)"
    gpgid=("gpg_1_$(randStr 8)")
    role_file="$PREFIX/$ROLE_DIR/$role"

    # mock fingerprint
    get_fingerprint () {
        echo "$1"
    }
    setupRoleExists "$role"

    assertFileEmpty "$role_file"
    cmd_role_assign "$role" "${gpgid[0]}"
    assertFileLinesEquals "$role_file" "${gpgid[@]}"

    gpgid+=("gpg_2_$(randStr 8)")
    cmd_role_assign "$role" "${gpgid[1]}"
    assertFileLinesEquals "$role_file" "${gpgid[@]}"

    gpgid+=("gpg_3_$(randStr 8)" "gpg_4_$(randStr 8)")
    cmd_role_assign "$role" "${gpgid[2]}" "${gpgid[3]}"
    assertFileLinesEquals "$role_file" "${gpgid[@]}"
}

setupRoleAssigned () {
    local role="$1"; shift
    local gpgid=("$@")
    setupRoleExists "$role"
    cmd_role_assign "$role" "${gpgid[@]}"
}

testCmdRoleUnassign () {
    local role gpgid role_file
    role="testrole_$(randStr 8)"
    gpgid=("gpg_1_$(randStr 8)" "gpg_2_$(randStr 8)" "gpg_3_$(randStr 8)")
    role_file="$PREFIX/$ROLE_DIR/$role"
    setupRoleAssigned "$role" "${gpgid[@]}"

    assertFileLinesEquals "$role_file" "${gpgid[@]}"

    cmd_role_unassign "$role" "${gpgid[1]}"

    # second one has been removed
    assertFileLinesEquals "$role_file" "${gpgid[0]}" "${gpgid[2]}"
}

testIsRole () {
    local role
    role="testrole_1_$(randStr 8)"
    setupRoleExists "$role"
    assertFileExists "${PREFIX}/$ROLE_DIR/$role"
    assertTrue "role should exist" "is_role $role"
    assertFalse "role should not exist" "is_role nonexisting"
}

testResolvePgpids () {
    die () {
        echo "ERROR: $1"
    }

    local roles=("testrole_0_$(randStr 8)" "testrole_1_$(randStr 8)")
    local gpgids=("gpg_0_$(randStr 8)" "gpg_1_$(randStr 8)" "gpg_2_$(randStr 8)")

    setupRoleAssigned "${roles[0]}" "${gpgids[0]}" "${gpgids[1]}"
    # nested roles
    # setupRoleAssigned "${roles[0]}" "${gpgids[0]}" "${gpgids[1]}"  "${roles[1]}"
    setupRoleAssigned "${roles[1]}" "${gpgids[2]}"

    local role_file="$PREFIX/$ROLE_DIR/${roles[0]}"
    assertFileLinesEquals "$role_file" "${gpgids[0]}" "${gpgids[1]}"
    # nested roles
    # assertFileLinesEquals "$role_file" "${gpgids[0]}" "${gpgids[1]}" "${roles[1]}"
    local role_file="$PREFIX/$ROLE_DIR/${roles[1]}"
    assertFileLinesEquals "$role_file" "${gpgids[2]}"


    local resolved
    resolved="$(resolve_gpgids "${roles[0]}")"
    assertEquals "first two gpg-ids are in the first role" "${gpgids[0]} ${gpgids[1]}" "${resolved//$'\n'/ }"
    # nested roles
    # assertEquals "$(echo ${gpgids[*]})" "${resolved}"
    resolved="$(resolve_gpgids "${roles[1]}")"
    assertEquals "${gpgids[2]}" "${resolved//$'\n'/ }"
}

testIsSharedDir () {
    local shared shared_dir shared_hint_file
    shared="testdir_$(randStr 8)/testsub_$(randStr 8)"
    shared_dir="$PREFIX/$shared"
    shared_hint_file="$shared_dir/${SHARED_HINT_FILE}"

    mkdir -p "$shared_dir"
    touch "$shared_hint_file"
    assertTrue "is_shared_dir $shared" "is_shared_dir $shared"
    assertTrue "is_shared_dir $shared_dir" "is_shared_dir $shared_dir"
    assertFalse "! is_shared_dir blub" "is_shared_dir blub"
}

testListSharedHintFiles () {
    local shared shared_dir shared_hint_file
    shared="testdir_1_$(randStr 8)/testsub_1_$(randStr 8)"
    shared_dir="$PREFIX/$shared"
    shared_hint_file="$shared_dir/${SHARED_HINT_FILE}"

    mkdir -p "$shared_dir"
    touch "$shared_hint_file"

    local shared_2 shared_dir_2 shared_hint_file_2 
    shared_2="testdir_2_$(randStr 8)/testsub_2_$(randStr 8)"
    shared_dir_2="$PREFIX/$shared_2"
    shared_hint_file_2="$shared_dir_2/${SHARED_HINT_FILE}"

    mkdir -p "$shared_dir_2"
    touch "$shared_hint_file_2"

    local found expected sort_found sort_expected
    IFS=' '
    read -r -a found <<< "$(list_shared_hint_files)"
    read -r -a expected <<< "$shared_hint_file_2 $shared_hint_file"
    IFS=$'\n'
    sort_found=("$(sort <<<"${found[*]}")")
    sort_expected=("$(sort <<< "${expected[*]}")")
    unset IFS

    assertEquals "found shared hint files" "${sort_expected[*]}" "${sort_found[*]}"
}

testUnsetShare () {
    #mock reencrypt_path
    reencrypt_path () {
        return 0
    }
    local shared shared_dir shared_hint_file
    shared="testdir_$(randStr 8)/testsub_$(randStr 8)"
    shared_dir="$PREFIX/$shared"
    shared_hint_file="$shared_dir/${SHARED_HINT_FILE}"

    mkdir -p "$shared_dir"
    touch "$shared_hint_file"
    assertTrue "is_shared_dir $shared" "is_shared_dir $shared"
    assertTrue "is_shared_dir $shared_dir" "is_shared_dir $shared_dir"

    unset_share "$shared"
    assertFalse "$shared_hint_file is empty" "[ -s \"$shared_hint_file\" ]"
    assertTrue "$shared_hint_file exists" "[ -f \"$shared_hint_file\" ]"

    update_single_share "$shared_dir"
    assertFalse "! is_shared_dir $shared" "is_shared_dir $shared"
    assertFalse "! is_shared_dir $shared_dir" "is_shared_dir $shared_dir"
}

testAddSingleShare () {
    local shared shared_dir shared_hint_file
    shared="testdir_$(randStr 8)/testsub_$(randStr 8)"
    shared_dir="$PREFIX/$shared"
    shared_hint_file="$shared_dir/${SHARED_HINT_FILE}"


    assertFileNotExists "$shared_dir"
    assertFileNotExists "$shared_hint_file"
    assertFalse "! is_shared_dir $shared" "is_shared_dir $shared"
    assertFalse "! is_shared_dir $shared_dir" "is_shared_dir $shared_dir"
    add_single_share "$shared_dir" "role1"

    assertTrue "is_shared_dir $shared" "is_shared_dir $shared"
    assertTrue "is_shared_dir $shared_dir" "is_shared_dir $shared_dir"

    assertDirExists "$shared_dir"
    assertFileExists "$shared_hint_file"
    assertFileNotEmpty "$shared_hint_file"
    assertFileLinesEquals "$shared_hint_file" "role1"

    add_single_share "$shared_dir" "role1"
    assertFileLinesEquals "$shared_hint_file" "role1"

    add_single_share "$shared_dir" "role2"
    assertFileLinesEquals "$shared_hint_file" "role1" "role2"

    add_single_share "$shared_dir" "role1"
    assertFileLinesEquals "$shared_hint_file" "role1" "role2"

    add_single_share "$shared_dir" "role3"
    assertFileLinesEquals "$shared_hint_file" "role1" "role2" "role3"

    add_single_share "$shared_dir" "role2"
    assertFileLinesEquals "$shared_hint_file" "role1" "role2" "role3"
}

testCheckRoleNameOk () {
    local out
    out=$(check_role_name_ok "asdf0123-._ASDF" 2>&1)
    assertEquals "empty out" "" "$out"
    out=$(check_role_name_ok "a")
    assertEquals "empty out" "" "$out"
    out=$(check_role_name_ok ".asdf")
    assertEquals "ERROR out" "Role names must not start or end with a dot." "$out"
    out=$(check_role_name_ok "---")
    assertEquals "ERROR out" "Role names must contain at least one character from the latin alphabet." "$out"
    out=$(check_role_name_ok "asdf:asdf")
    assertEquals "ERROR out" "Role names must not contain any other characters than latin alphabetic characters, arabic numbers, '-', '_', and '.'." "$out"
    out=$(check_role_name_ok "asdf&asdf")
    assertEquals "ERROR out" "Role names must not contain any other characters than latin alphabetic characters, arabic numbers, '-', '_', and '.'." "$out"
    out=$(check_role_name_ok "asdf asdf")
    assertEquals "ERROR out" "Role names must not contain any other characters than latin alphabetic characters, arabic numbers, '-', '_', and '.'." "$out"
    out=$(check_role_name_ok "asdf/asdf")
    assertEquals "ERROR out" "Role names must not contain any other characters than latin alphabetic characters, arabic numbers, '-', '_', and '.'." "$out"
    out=$(check_role_name_ok "" "Usage: BLA")
    assertEquals "ERROR out" "Role name is missing. Usage: BLA" "$out"
}

testVersion () {
    # shellcheck disable=SC2034
    PASS_TEAM_QUIET=0
    local out version="$PASS_TEAM_VERSION"
    out="$(team_main --version)"
    assertTrue "human-readable version 1" "echo \"${out}\" | grep -c \"=.*${version}.*=\""

    out="$(team_main --version blabla)"
    assertTrue "human-readable version 2" "echo \"${out}\" | grep -c \"=.*${version}.*=\""

    out="$(team_main -q --version)"
    assertEquals "machine-readable version 1" "${version}" "${out}"

    out="$(team_main --version -q)"
    assertEquals "machine-readable version 2" "${version}" "${out}"
}

testHelp () {
    local out
    out="$(team_main)"
    assertTrue "usage 1" "echo \"${out}\" | grep -c \"Usage:\""

    out="$(team_main blabla)"
    assertTrue "usage 2" "echo \"${out}\" | grep -c \"Usage:\""

    out="$(team_main list blabla )"
    assertTrue "usage 3" "echo \"${out}\" | grep -c \"Usage:\""

    out="$(team_main help blabla )"
    assertTrue "usage 4" "echo \"${out}\" | grep -c \"Usage:\""
}

testShareHelp () {
    local out
    out="$(team_main share)"
    assertTrue "usage 1" "echo \"${out}\" | grep -c \"Usage:\""

    out="$(team_main share blabla)"
    assertTrue "usage 2" "echo \"${out}\" | grep -c \"Usage:\""

    out="$(team_main share help)"
    assertTrue "usage 3" "echo \"${out}\" | grep -c \"Usage:\""
}

testRoleHelp () {
    local out
    out="$(team_main role)"
    assertTrue "usage 1" "echo \"${out}\" | grep -c \"Usage:\""

    out="$(team_main role blabla)"
    assertTrue "usage 2" "echo \"${out}\" | grep -c \"Usage:\""

    out="$(team_main role help)"
    assertTrue "usage 3" "echo \"${out}\" | grep -c \"Usage:\""
}

testDebugLevel () {
    assertEquals "level 0" "0" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    cmd_team -d --version &> /dev/null
    assertEquals "level 1a" "1" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
    cmd_team --debug --version &> /dev/null
    assertEquals "level 1b" "1" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
    cmd_team -dd --version &> /dev/null
    assertEquals "level 2a" "2" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
    cmd_team --debug --debug --version &> /dev/null
    assertEquals "level 2b" "2" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
    cmd_team -ddd --version &> /dev/null
    assertEquals "level 3a" "3" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
    cmd_team -dddd --version &> /dev/null
    assertEquals "level 3b" "3" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
    cmd_team -d -d -d --debug --version &> /dev/null
    assertEquals "level 3c" "3" "$PASSWORD_STORE_TEAM_DEBUG_LEVEL"
    PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
}

testLicense () {
    local out
    out="$(team_main --license)"
    assertTrue "License GPLv3 or later" "echo \"${out}\" | grep -c \"GPLv3 or later\""
}

testCmdShareReencrypt () {
    export PASS_TEAM_QUIET=0

    # mock resolve_gpgids
    resolve_gpgids () {
        echo "$1"
    }

    # mimic an initialized team
    mkdir -p "$PREFIX/$PASSWORD_STORE_TEAM_DIR"
    echo "manager1" > "$PREFIX/$PASSWORD_STORE_TEAM_DIR/.gpg-id"
    expect_initialized

    # setup test shared directory
    local shared shared_dir shared_hint_file
    shared="testdir_$(randStr 8)/testsub_$(randStr 8)"
    shared_dir="$PREFIX/$shared"
    shared_hint_file="$shared_dir/${SHARED_HINT_FILE}"

    assertFileNotExists "$shared_dir"
    assertFileNotExists "$shared_hint_file"
    assertFalse "! is_shared_dir $shared" "is_shared_dir $shared"
    assertFalse "! is_shared_dir $shared_dir" "is_shared_dir $shared_dir"

    local out
    out="$(cmd_share_reencrypt 2>&1)"
    assertTrue "usage" "echo \"${out}\" | grep -c \"Missing <directory> argument.\""
    out="$(cmd_share_reencrypt "$shared" 2>&1)"
    assertTrue "not shared" "echo \"${out}\" | grep -c \"'$shared' is not a shared directory.\""

    add_single_share "$shared_dir" "role1"
    assertTrue "is_shared_dir $shared" "is_shared_dir $shared"
    assertTrue "is_shared_dir $shared_dir" "is_shared_dir $shared_dir"
    assertFileLinesEquals "$shared_hint_file" "role1"

    # mock reencrypt_path
    reencrypt_path () {
        echo "reencrypt_path $1"
    }

    out="$(cmd_share_reencrypt "$shared" 2>&1)"
    assertTrue "reencrypt_path called" "echo \"${out}\" | grep -c \"reencrypt_path $shared_dir\""
}

# shellcheck disable=SC1091
source shunit2
