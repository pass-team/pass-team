#!/bin/bash

[ -z "${PASS_TEAM_INTEGRATION_TESTS}" ] && echo "PASS_TEAM_INTEGRATION_TESTS environmal variable is unset. Will not run the integration tests. Use 'make inttest.env && source inttest.env' to create a proper environment for the integration tests." && exit 1

source test/utils.bash
export PASSWORD_STORE_TEAM_DEBUG_LEVEL=0
export PASSWORD_STORE_TEAM_QUIET=

declare -A _ROLES
_ROLES=([humanresources]="passt-user1 passt-user5"
    [customerrelations]="passt-user2 passt-user5"
    [development]="passt-user3 passt-user5"
    [marketing]="passt-user4 passt-user5"
    [boss]="passt-user5")

tearDown () {
    # remove generated data
    rm -r "$PASSWORD_STORE_DIR/.team"
    for role in "${!_ROLES[@]}" ; do
        rm -r "$PASSWORD_STORE_DIR/$role-dir"
    done
}

initWithTwoManagers () {
    get_fingerprint() {
        gpg -k --with-colons "$1" 2>/dev/null | awk -F: '$1 == "fpr" {print $10;}' | head -n1
    }

    pass init "passt-user1"
    local gpgid=("$(get_fingerprint 'passt-manager1')" "$(get_fingerprint 'passt-manager2')")
    pass team init "${gpgid[@]}"
    assertFileLinesEquals "$PASSWORD_STORE_DIR/.team/.gpg-id" "${gpgid[@]}"
    assertFileNotExists "$PASSWORD_STORE_DIR/.team/.almighty-gpg-id"
}

insertRoles () {
    for role in "${!_ROLES[@]}" ; do
        pass team role insert "$role"
        assertFileExists "$PASSWORD_STORE_DIR/.team/role/$role"
        assertFileEmpty "$PASSWORD_STORE_DIR/.team/role/$role"
    done
}

assignRoles () {
    for role in "${!_ROLES[@]}" ; do
        local gpgids
        IFS=' '
        read -r -a gpgids <<< "${_ROLES[$role]}"
        unset IFS
        pass team role assign "$role" "${gpgids[@]}"
        assertFileNotEmpty "$PASSWORD_STORE_DIR/.team/role/$role"
        local finger_prints gpgid sort_finger_prints
        finger_prints=()
        for gpgid in "${gpgids[@]}"; do
            finger_prints+=("$(gpg -k --with-colon "$gpgid" | awk -F: '$1 == "fpr" {print $10;}' | head -n1)")
        done


        readarray -d $'\0' -t sort_finger_prints < <(printf '%s\0' "${finger_prints[@]}" | sort -z)
        assertFileLinesEquals "$PASSWORD_STORE_DIR/.team/role/$role" "${sort_finger_prints[@]}"
    done

    # assign non-existing role
    # quiet
    local stderr exit_code
    stderr="$(pass team role assign blablablub passt-user1 2>&1)"
    exit_code=$?
    assertEquals "exit code is EXIT_ROLE_DOES_NOT_EXIST" "86" "$exit_code"
    assertEquals "stderr is empty" "" "$stderr"

    # not quiet, but --no-color
    unset PASSWORD_STORE_TEAM_QUIET
    stderr="$(PASSWORD_STORE_TEAM_NO_COLOR=1 pass team role assign blablablub passt-user1 2>&1)"
    exit_code=$?
    assertEquals "exit code is EXIT_ROLE_DOES_NOT_EXIST" "86" "$exit_code"
    assertEquals "stderr is not empty" "$(echo -e "Error: The role 'blablablub' does not exist.\\n       Please add it to your pass team.")" "$stderr"

    export PASSWORD_STORE_TEAM_QUIET=
}

listRoles () {
    # list roles
    assertEquals "list roles" "boss customerrelations development humanresources marketing" "$(pass team role list)"

    # list roles, not quiet, but no color
    unset PASSWORD_STORE_TEAM_QUIET
    assertEquals "contains 'Roles'" "1" "$(PASSWORD_STORE_NO_COLOR=1 pass team role list | grep -c "Roles")"

    export PASSWORD_STORE_TEAM_QUIET=
}


showRoles () {
    # show role
    # usage error
    stderr="$(pass team -q role show 2>&1)"
    exit_code=$?
    assertEquals "exit code is EXIT_USAGE_ERROR" "81" "$exit_code"
    assertEquals "stderr is empty (-q)" "" "$stderr"

    local stdout
    stdout="$(pass team --no-color role show "${!_ROLES[@]}")"
    assertEquals "Role header" "1" "$(echo "$stdout" | grep -c "Role: development")"
    assertEquals "GPG Keys" "5" "$(echo "$stdout" | grep -c "└── GPG Keys:")"

}

removeRoles () {

    # remove role
    stderr="$(pass team -q role remove bla 2>&1)"
    exit_code=$?
    assertEquals "exit code is EXIT_ROLE_DOES_NOT_EXIST" "86" "$exit_code"
    assertEquals "stderr is empty (-q)" "" "$stderr"

    pass team role create dummy
    pass team role assign dummy passt-user1

    stderr="$(pass team -q role remove dummy 2>&1)"
    exit_code=$?
    assertEquals "exit code is EXIT_ROLE_IS_ASSIGNED_TO_SUBJECTS" "85" "$exit_code"
    assertEquals "stderr is empty (-q)" "" "$stderr"

    pass team -q role unassign dummy passt-user1

    unset PASSWORD_STORE_TEAM_QUIET
    stdout="$(pass team role remove dummy)"
    assertEquals "stdout contains 'Removed role dummy'" "Removed role 'dummy'." "$stdout"
    export PASSWORD_STORE_TEAM_QUIET=
}


unassingRoles () {
    pass team role create dummy

    unset PASSWORD_STORE_TEAM_QUIET
    stderr="$(pass team role unassign dummy unknown 2>&1)"
    exit_code=$?
    assertEquals "exit code is EXIT_UNKNOWN_GPG_PUBLIC_KEY" "84" "$exit_code"
    assertEquals "stderr contains 'public key unknown'" "1" "$(echo "$stderr" | grep -c "public key is unknown")"

    pass team -q role assign dummy passt-user1

    stdout="$(pass team role unassign dummy passt-user1)"
    assertEquals "stdout contains 'public key unknown'" "Unassigned role 'dummy' from subject 'passt-user1'." "$stdout"
    export PASSWORD_STORE_TEAM_QUIET=

    pass team role remove dummy
}

createRolesDirs () {
    for role in "${!_ROLES[@]}" ; do
        pass team share set "$role-dir" "$role"
        assertFileLinesEquals "$PASSWORD_STORE_DIR/$role-dir/.team.shared" "$role"
    done

    # share with empty role
    pass team role create dummy
    unset PASSWORD_STORE_TEAM_QUIET
    stderr="$(pass team --no-color share set dummy-dir dummy 2>&1)"
    assertEquals "stderr contains warning: empty role" "1" "$(echo "$stderr" | grep -c "Warning: You are sharing a directory with a role which is not assigned to any subject.")"
    export PASSWORD_STORE_TEAM_QUIET=
    pass team share unset dummy-dir
    pass team role remove dummy

}

addBossRoleToShare () {
    for role in "${!_ROLES[@]}" ; do
        pass team share add "$role-dir" "boss"
        assertFileLinesEquals "$PASSWORD_STORE_DIR/$role-dir/.team.shared" "boss" "$role"
    done
}

removeBossRoleFromShare () {
    for role in "${!_ROLES[@]}" ; do
        [ "$role" = "boss" ] && continue

        pass team share rm "$role-dir" "boss"
        assertFileLinesEquals "$PASSWORD_STORE_DIR/$role-dir/.team.shared" "$role"
    done
}


insertSharedSecrets () {
    local gensecret readsecret
    for role in "${!_ROLES[@]}" ; do
        gensecret="$(pass generate "$role-dir/testsec" --no-symbols)"
        if [[ "$role" == "humanresources" ]] ; then
            # we have the key for this directory
            readsecret="$(pass show "$role-dir/testsec")"
            assertEquals "pass show should exit w/o errors: $readsecret" "0" "$?"
            assertTrue "secrets $gensecret and $readsecret should match" "[[ '$gensecret' == *'$readsecret'* ]]"
        else
            # we don't have the key for this directory
            local code msg
            msg="$(pass show "$role-dir/testsec" 2>&1)"
            code="$?"
            assertEquals "pass show should exit with code 2" "2" "$code"
            assertEquals "pass show should exit error: no secret key " "gpg: decryption failed: No secret key" "$msg"
        fi
    done
}


testScenario1 () {
    echo "--initWithTwoManagers"
    initWithTwoManagers
    echo "--insertRoles"
    insertRoles
    echo "--assignRoles"
    assignRoles
    echo "--listRoles"
    listRoles
    echo "--showRoles"
    showRoles
    echo "--unassingRoles"
    unassingRoles
    echo "--removeRoles"
    removeRoles
    echo "--createRolesDirs"
    createRolesDirs
    echo "--addBossRoleToShare"
    addBossRoleToShare
    echo "--remove boss share"
    pass team share unset "boss-dir"
    echo "--removeBossRoleFromShare"
    removeBossRoleFromShare
    echo "--reset RolesDirs"
    createRolesDirs
    echo "--insertSharedSecrets"
    insertSharedSecrets
    echo "--remove passt-user5, reencryption fails"
    local error
    unset PASSWORD_STORE_TEAM_QUIET
    error="$(pass team role unassign customerrelations passt-user5 2>&1)"
    assertTrue "Reencryption should fail: $error" "[[ \"$error\" == *\"gpg: decryption failed: No secret key\"* ]]"
    echo "--end"
}

# shellcheck disable=SC1091
source shunit2
