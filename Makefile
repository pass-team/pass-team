SHELL:=/bin/bash

DESTDIR ?=
prefix ?= /usr
datarootdir ?= $(prefix)/share
mandir ?= $(datarootdir)/man

BASHCOMPDIR ?= $(datarootdir)/bash-completion/completions

VERSION=$(shell cat VERSION)

README=README.md
MAN_SOURCES_ROFF=man/pass-team.1
MAN_SOURCES_MD=man/pass-team.md
EXTENSION_SOURCES=src/team.bash
BASH_COMPLETION_SOURCES=src/team.completion.bash
DEBIAN_CHANGELOG=debian/changelog
SOURCES=$(EXTENSION_SOURCES) $(BASH_COMPLETION_SOURCES) $(MAN_SOURCES_ROFF) $(MAN_SOURCES_MD)

ALL: $(README) $(MAN_SOURCES_ROFF) $(EXTENSION_SOURCES)-version $(DEBIAN_CHANGELOG)

$(EXTENSION_SOURCES)-version: VERSION $(EXTENSION_SOURCES)
	@sed -i "s/^PASS_TEAM_VERSION=.*$$/PASS_TEAM_VERSION='$$(cat VERSION)'/" $(EXTENSION_SOURCES)

#######################################
# Installation
#######################################

INSTALL ?= install

ifeq ($(WITH_BASHCOMP),)
ifneq ($(strip $(wildcard $(BASHCOMPDIR))),)
WITH_BASHCOMP:=yes
endif
endif

ifeq ($(WITH_MAN),)
ifneq ($(strip $(wildcard $(mandir))),)
	WITH_MAN:=yes
endif
endif

PASSWORD_STORE_EXECUTABLE ?= $(shell command -v pass || true)
ifneq ($(PASSWORD_STORE_EXECUTABLE),)
PASSWORD_STORE_EXTENSIONS_INSTALL_DIR ?= $(shell cat "$(PASSWORD_STORE_EXECUTABLE)" | grep "SYSTEM_EXTENSION_DIR=" | sed 's/SYSTEM_EXTENSION_DIR="\(.*\)"/\1/')
endif

.PHONY: installcheck
installcheck:
	@if [ -z "$(PASSWORD_STORE_EXECUTABLE)" ] ; then \
		>&2 echo 'Warning: The `pass` executable has not been found. Is it installed? Provide the path via the "PASSWORD_STORE_EXECUTABLE" variable to supress this warnin.' ; \
	fi
	@if [ -z "$(PASSWORD_STORE_EXTENSIONS_INSTALL_DIR)" ] ; then \
		>&2 echo 'The directory for the pass extensions could not be determined. Please provide it via the "PASSWORD_STORE_EXTENSIONS_INSTALL_DIR" variable.' \
		exit 3 ; \
	fi

install: installcheck install-man install-completion $(EXTENSION_SOURCES)
	@if [ ! -d "$(PASSWORD_STORE_EXTENSIONS_INSTALL_DIR)" ] ; then \
		$(INSTALL) -v -d "$(PASSWORD_STORE_EXTENSIONS_INSTALL_DIR)" ; \
	fi
	$(INSTALL) -m0755 -v "$(EXTENSION_SOURCES)" "$(PASSWORD_STORE_EXTENSIONS_INSTALL_DIR)/team.bash"

install-completion: install-completion-bash

install-completion-bash: $(BASH_COMPLETION_SOURCES)
	@if [ "$(WITH_BASHCOMP)" = "yes" ] ; then \
		$(INSTALL) -v -d "$(DESTDIR)$(BASHCOMPDIR)" ; \
		$(INSTALL) -m 0644 -v "$(BASH_COMPLETION_SOURCES)" "$(DESTDIR)$(BASHCOMPDIR)/pass-team" ; \
	fi

install-man: $(MAN_SOURCES_ROFF)
	@if [ "$(WITH_MAN)" = "yes" ] ; then \
		$(INSTALL) -v -d "$(DESTDIR)$(mandir)/man1" && $(INSTALL) -m 0644 -v "$(MAN_SOURCES_ROFF)" "$(DESTDIR)$(mandir)/man1/pass-team.1" ; \
	fi


#######################################
# Man Page
#######################################

PANDOC_EXECUTABLE ?= $(shell command -v pandoc || true)

ifneq ($(PANDOC_EXECUTABLE),)
$(MAN_SOURCES_ROFF): $(MAN_SOURCES_MD)
	echo '.\" Auto-generated file. DO NOT CHANGE THIS. Change the markdown file instead.' > $@
	$(PANDOC_EXECUTABLE) -s -f markdown -t man "$<" >> $@
endif

$(MAN_SOURCES_MD): VERSION RELEASE
	sed -i "s/^footer: Pass Team .*$$/footer: Pass Team $$(cat VERSION)/" $(MAN_SOURCES_MD)
	source RELEASE ; sed -i "s/^date: .*$$/date: $$RELEASE_DATE/g" $(MAN_SOURCES_MD)


#######################################
# Test Environment
#######################################

PLAYGROUND_DIR ?= /tmp/playground
PLAYGROUND_ENV = $(PLAYGROUND_DIR)/source.me

PLAYGROUND_GPG_HOME = $(PLAYGROUND_DIR)/.gnupg
PLAYGROUND_GPG=gpg --homedir=$(PLAYGROUND_GPG_HOME)
PLAYGROUND_GPG_GENERATE=$(PLAYGROUND_GPG) --quick-generate-key --batch --yes --passphrase ''
PLAYGROUND_GPG_DELETE_SECRET_KEY=$(PLAYGROUND_GPG) --batch --yes --delete-secret-keys
EXTRACT_FPR=grep -m 1 "^fpr" | sed -e "s/fpr\:*\([0-9A-F]*\)\:/\1/g"

PLAYGROUND_PASSWORD_STORE_DIR=$(PLAYGROUND_DIR)/.password-store
PLAYGROUND_EXTENSIONS_DIR=$(PLAYGROUND_PASSWORD_STORE_DIR)/.extensions

playground: $(PLAYGROUND_ENV) $(PLAYGROUND_GPG_HOME)
	$(MAKE) PASSWORD_STORE_EXTENSIONS_INSTALL_DIR="$(PLAYGROUND_EXTENSIONS_DIR)" WITH_BASHCOMP=no WITH_MAN=no install

$(PLAYGROUND_DIR):
	mkdir -p $(PLAYGROUND_DIR)

$(PLAYGROUND_GPG_HOME):
	@echo "generate keys for dev password store."
	@$(PLAYGROUND_GPG_GENERATE) passt-manager1
	@$(PLAYGROUND_GPG_GENERATE) passt-manager2
	@$(PLAYGROUND_GPG_GENERATE) passt-user1
	@$(PLAYGROUND_GPG_GENERATE) passt-user2
	@$(PLAYGROUND_GPG_GENERATE) passt-user3
	@$(PLAYGROUND_GPG_GENERATE) passt-user4
	@$(PLAYGROUND_GPG_GENERATE) passt-user5
	@# drop secret keys for some
	@$(PLAYGROUND_GPG_DELETE_SECRET_KEY) $$($(PLAYGROUND_GPG) -k --with-colons passt-manager2 | $(EXTRACT_FPR))
	@$(PLAYGROUND_GPG_DELETE_SECRET_KEY) $$($(PLAYGROUND_GPG) -k --with-colons passt-user2 | $(EXTRACT_FPR))
	@$(PLAYGROUND_GPG_DELETE_SECRET_KEY) $$($(PLAYGROUND_GPG) -k --with-colons passt-user3 | $(EXTRACT_FPR))
	@$(PLAYGROUND_GPG_DELETE_SECRET_KEY) $$($(PLAYGROUND_GPG) -k --with-colons passt-user4 | $(EXTRACT_FPR))
	@$(PLAYGROUND_GPG_DELETE_SECRET_KEY) $$($(PLAYGROUND_GPG) -k --with-colons passt-user5 | $(EXTRACT_FPR))

$(PLAYGROUND_ENV): $(PLAYGROUND_DIR)
	@echo "export PASSWORD_STORE_DIR=$(PLAYGROUND_PASSWORD_STORE_DIR)" > $@
	@echo "export PASSWORD_STORE_ENABLE_EXTENSIONS=true" >> $@
	@echo "export GNUPGHOME=$(PLAYGROUND_GPG_HOME)" >> $@
	@echo "export PASS_TEAM_INTEGRATION_TESTS=ENABLED" >> $@
	@echo "export FILE_UNDER_TEST=$(PLAYGROUND_EXTENSIONS_DIR)/team.bash" >> $@
	@echo "export TEST_DIR=$(PLAYGROUND_PASSWORD_STORE_DIR)" >> $@
	@echo 'PS1="(pass team playground) $$PS1"' >> $@
	@echo "source src/team.completion.bash" >> $@


#######################################
# Tests
#######################################

INTTEST_DIR ?= $$(mktemp -d)
CLEAN_INTTEST_TARGET ?= clean-playground
COVERAGE=
KCOV_OPTS=--exclude-pattern=/usr/ --exclude-path=./test
DEFAULT_COVERAGE_OUTDIR=.coverage
COVERAGE_OUTDIR ?=
KCOV_EXECUTABLE ?= $(shell command -v kcov || true)
ifneq ($(KCOV_EXECUTABLE),)
ifneq ($(COVERAGE_OUTDIR),)
COVERAGE=$(KCOV_EXECUTABLE) $(KCOV_OPTS) $(COVERAGE_OUTDIR)
endif
endif
SHELLCHECK=shellcheck

.PHONY: check
check: test

.PHONY: test
test: linting unit-tests integration-tests completion-tests

.PHONY: linting
linting: src/* test/*
	$(SHELLCHECK) -s bash src/* test/*

.PHONY: unit-tests
unit-tests: test/utils.bash test/unit-tests.bash coverage-out-dir
	$(COVERAGE) ./test/unit-tests.bash ; \

.PHONY: integration-tests
integration-tests: test/utils.bash test/integration-tests.bash coverage-out-dir
	trap "$(MAKE) $(CLEAN_INTTEST_TARGET)" EXIT ; \
	export PLAYGROUND_DIR="$(INTTEST_DIR)" && \
	$(MAKE) playground && \
	source "$$PLAYGROUND_DIR/source.me" && \
	$(COVERAGE) ./test/integration-tests.bash

.PHONY: completion-tests
completion-tests: bash-completion-tests

.PHONY: bash-completion-tests
bash-completion-tests: $(BASH_COMPLETION_SOURCES) test/utils.bash test/completion-tests.bash coverage-out-dir
	trap "$(MAKE) $(CLEAN_INTTEST_TARGET)" EXIT ; \
	export PLAYGROUND_DIR="$(INTTEST_DIR)" && \
	$(MAKE) playground && \
	source "$$PLAYGROUND_DIR/source.me" && \
	$(COVERAGE) ./test/completion-tests.bash

.PHONY: coverage-out-dir
coverage-out-dir:
	@if [ -n "$(COVERAGE)" ] ; then \
		if [ -n "$(COVERAGE_OUTDIR)" ] ; then \
			mkdir -p "$(COVERAGE_OUTDIR)" ; \
		fi \
	fi

.PHONY: coverage
coverage:
	kcov --version
	export INTTEST_DIR="$(INTTEST_DIR)" && \
	export FILE_UNDER_TEST="$${INTTEST_DIR}/.password-store/.extensions/team.bash" && \
	export PLAYGROUND_DIR="$${INTTEST_DIR}" && \
	$(MAKE) INTTEST_DIR="$${INTTEST_DIR}" COVERAGE_OUTDIR=$$(pwd)/$(DEFAULT_COVERAGE_OUTDIR) playground unit-tests integration-tests completion-tests
	@echo -e "\n\n\nDone. Please find the coverage report in $$(pwd)/$(DEFAULT_COVERAGE_OUTDIR)/index.html"

#######################################
# Homebrew Formula
#######################################

BUILD_DIR=build
HOMEBREW_BUILD_DIR=$(BUILD_DIR)/homebrew
HOMEBREW_TARBALL=$(HOMEBREW_BUILD_DIR)/pass-team-$(VERSION).tar.gz
HOMEBREW_FORMULA_TEMPLATE=homebrew/pass-team.template.rb
HOMEBREW_FORMULA=$(HOMEBREW_BUILD_DIR)/pass-team.rb
HOMEBREW_URL ?= file://$(shell pwd)/$(HOMEBREW_TARBALL)

.PHONY: $(HOMEBREW_FORMULA)
$(HOMEBREW_FORMULA): $(HOMEBREW_FORMULA_TEMPLATE) $(HOMEBREW_TARBALL)
	sed "s#sha256 \"\"#$$(shasum -b -a 256 $(HOMEBREW_TARBALL) | awk '{printf "sha256 \"%s\"", $$1}')#" $(HOMEBREW_FORMULA_TEMPLATE) > $@
	sed "s|url \"\"|url \"$(HOMEBREW_URL)\"|" -i $@
	sed "s|version \"\"|version \"$(VERSION)\"|" -i $@

$(HOMEBREW_TARBALL): $(SOURCES) VERSION
	mkdir -p $(HOMEBREW_BUILD_DIR)
	tar -X .buildignore --exclude=debian -cvzSf "$@" .


#######################################
# Debian Package
#######################################

DEBIAN_BUILD_DIR=$(BUILD_DIR)/deb
DEBUILD_EXECUTABLE ?= $(shell command -v debuild || true)
DEBIAN_VERSION=$(VERSION)
DEBIAN_DISTRIBUTION=unstable
DEBIAN_CHANGELOG_DESCRIPTION=TODO
ifeq ($(DEBIAN_VERSION),dev)
DEBIAN_VERSION=9999-dev
DEBIAN_DISTRIBUTION=experimental
DEBIAN_CHANGELOG_DESCRIPTION=This is a build of the unreleased dev branch.
endif
DEBIAN_REVISION=1
DEBIAN_TARBALL=$(DEBIAN_BUILD_DIR)/pass-extension-team_$(DEBIAN_VERSION).orig.tar.gz

.PHONY: debian-package
debian-package: check-debuild $(DEBIAN_TARBALL)
	cd $(DEBIAN_BUILD_DIR)/pass-extension-team_$(DEBIAN_VERSION) \
		&& tar -xvzf "../pass-extension-team_$(DEBIAN_VERSION).orig.tar.gz" \
		&& $(DEBUILD_EXECUTABLE) -e PASSWORD_STORE_EXTENSIONS_INSTALL_DIR=$$(pwd)/debian/pass-extension-team/usr/lib/password-store/extensions -us -uc

$(DEBIAN_TARBALL): $(SOURCES) VERSION
	mkdir -p $(DEBIAN_BUILD_DIR)/pass-extension-team_$(DEBIAN_VERSION)/
	tar -X .buildignore -cvzSf $@ .

$(DEBIAN_CHANGELOG): VERSION
	if ! grep -c "($(DEBIAN_VERSION)-$(DEBIAN_REVISION))" $@ ; then \
		sed -i -e "1i pass-extension-team ($(DEBIAN_VERSION)-$(DEBIAN_REVISION)) $(DEBIAN_DISTRIBUTION); urgency=low\n\n\
	  * $(DEBIAN_CHANGELOG_DESCRIPTION)\n\n\
	 -- $$(git config user.name) <$$(git config user.email)>  $$(date "+%a, %-d %b %Y %T %z")\n" \
		$@ ; \
	fi

.PHONY: check-debuild
check-debuild:
	@if [ -z "$(DEBUILD_EXECUTABLE)" ] ; then >&2 echo "Error: Command not found: debuild. Please install the devscripts and the debhelper-compat packages." && exit 127 ; fi


#######################################
# README
#######################################

DOCS_DIR=docs
README_SOURCES=$(addprefix $(DOCS_DIR)/, introduction.md installation_from_packages.md \
  installation_from_sources.md compatibility.md examples/README.md \
  tests.md ) CONTRIBUTING.md CREDITS.md LICENSE.md

$(README): $(README_SOURCES) VERSION
	@echo "<!-- DO NOT CHANGE THIS FILE. It is generated from *.md files in the repository. -->" > $@
	for f in $(README_SOURCES) ; do \
		if [ "$$f" = "$(DOCS_DIR)/examples/README.md" ] ; then \
			sed "s|./|./examples/|g" $$f | sed "s|(\.\/|(./$(DOCS_DIR)/|g" | sed "s|(\.\.\/|(./|g" >> $@ ; \
		elif case "$$f" in "$(DOCS_DIR)"*) true;; *) false;; esac; then \
			sed "s|(\.\/|(./$(DOCS_DIR)/|g" $$f | sed "s|(\.\.\/|(./|g" >> $@ ; \
		else \
			cat $$f >> $@ ; \
		fi ; \
		echo >> $@ ; \
	done

$(DOCS_DIR)/installation_from_packages.md: $(DEBIAN_CHANGELOG)
	sed "s|sudo apt-get install ./pass-extension-team.*$$|sudo apt-get install ./pass-extension-team_$$(head -n 1 $< | awk "-F[()]" '{print $$2}')_all.deb|g" -i $@


#######################################
# Examples
#######################################

REPOSITORY_URL ?= https://gitlab.com/pass-team/pass-team
REFERENCE_BRANCH ?= main
REPOSITORY_TREE=$(REPOSITORY_URL)/-/blob/$(REFERENCE_BRANCH)
EXAMPLES_DIR=$(DOCS_DIR)/examples
EXAMPLES=roles_management shared_password_directories read_and_write_passwords reencryption
EXAMPLES_MD=$(addsuffix .md, fresh_setup $(EXAMPLES))
EXAMPLES_BUILD_DIR ?= $$(mktemp -d)
CLEAN_EXAMPLES_BUILD_TARGET ?= clean-playground
BASH_TO_MARKDOWN ?= utils/bash-to-markdown/bash_to_markdown

examples: $(addprefix $(EXAMPLES_DIR)/, $(EXAMPLES_MD)) $(EXTENSION_SOURCES)

$(EXAMPLES_DIR)/fresh_setup.md: $(EXAMPLES_DIR)/fresh_setup.sh
	trap "$(MAKE) $(CLEAN_EXAMPLES_BUILD_TARGET)" EXIT ; \
	export PLAYGROUND_DIR="$(EXAMPLES_BUILD_DIR)" && \
	$(MAKE) playground && \
	source "$$PLAYGROUND_DIR/source.me" && \
	export PASSWORD_STORE_CHARACTER_SET='[:alnum:]' && \
	CONSOLE_ERR_HEADER="bash_err" CONSOLE_OUT_HEADER="bash_out" CONSOLE_IN_HEADER="bash" SOURCE=$(REPOSITORY_TREE)/$< $(BASH_TO_MARKDOWN) $< > $@ && \
	$(MAKE) -B EXAMPLES_BUILD_DIR="$$PLAYGROUND_DIR" $(addsuffix .md, $(addprefix $(EXAMPLES_DIR)/,$(EXAMPLES)))

$(EXAMPLES_DIR)/%.md: $(EXAMPLES_DIR)/%.sh
	export PASSWORD_STORE_CHARACTER_SET='[:alnum:]' && source $(PLAYGROUND_DIR)/source.me && CONSOLE_ERR_HEADER="bash_err" CONSOLE_OUT_HEADER="bash_out" CONSOLE_IN_HEADER="bash" SOURCE=$(REPOSITORY_TREE)/$< $(BASH_TO_MARKDOWN) $< > $@


#######################################
# PAGES
#######################################

PAGES_BASE_URL ?= https://pass-team.gitlab.io/pass-team/
PAGES_CONFIG=hugo/config.toml
CONTENT_TARGET=hugo/content
CONTENT_ARCHIVE=hugo/content.archive
CONTENT_TEMPLATE=hugo/content.template
DATA_TEMPLATE=hugo/data.template
DATA_TARGET=hugo/data
DATA_ARCHIVE=hugo/data.archive
VERSION_DATA_TARGET=$(DATA_TARGET)/$(VERSION)
VERSION_DATA_TEMPLATE=$(DATA_TEMPLATE)/__VERSION__
VERSION_DATA_CONTENTS=$(addprefix $(VERSION_DATA_TARGET)/, downloads.yaml)
VERSION_DOC_TARGET=$(CONTENT_TARGET)/$(VERSION)/$(DOCS_DIR)
VERSION_DOC_TEMPLATE=$(CONTENT_TEMPLATE)/__VERSION__/$(DOCS_DIR)
VERSION_DOC_CONTENTS=$(addprefix $(VERSION_DOC_TARGET)/, concepts.md \
  installation.md introduction.md man-pass-team.md)
OTHER_CONTENTS=$(addprefix $(CONTENT_TARGET)/, ROADMAP.md CREDITS.md CONTRIBUTING.md CODE_OF_CONDUCT.md LICENSE.md GPLv3.md VISION_AND_GOALS.md CHANGELOG.md)
MAN_PAGE_GIT=https://manpages.org/git
MAN_PAGE_GPG=https://manpages.org/gpg
MAN_PAGE_PASS=https://git.zx2c4.com/password-store/about/
VERSION_EXAMPLES_TEMPLATE=$(CONTENT_TEMPLATE)/__VERSION__/examples
VERSION_EXAMPLES_TARGET=$(CONTENT_TARGET)/$(VERSION)/examples
VERSION_EXAMPLE_CONTENTS=$(addprefix $(VERSION_EXAMPLES_TARGET)/, $(EXAMPLES_MD))
VERSION_INDEX=$(CONTENT_TARGET)/$(VERSION)/_index.md

pages: $(VERSION_INDEX) $(VERSION_DOC_CONTENTS) $(VERSION_EXAMPLE_CONTENTS) $(OTHER_CONTENTS) $(PAGES_CONFIG) $(VERSION_DATA_CONTENTS)

.PHONY: $(PAGES_CONFIG)
$(PAGES_CONFIG): VERSION RELEASE
	sed -i 's|baseurl\s*=.*$$|baseurl = "$(PAGES_BASE_URL)"|g' $@

$(VERSION_INDEX): $(CONTENT_TARGET)/$(VERSION) VERSION RELEASE
	echo "---" > $@
	source RELEASE ; echo "title: '$$RELEASE_NAME'" >> $@
	source RELEASE ; echo "summary: \"$$DESCRIPTION\"" >> $@
	source RELEASE ; echo "keywords: 'Pass, Team, Password Store, Share, Release, $(VERSION), $$RELEASE_NAME'" >> $@
	source RELEASE ; echo "publishdate: '$$RELEASE_DATE'" >> $@
	source RELEASE ; echo "lastmod: '$$RELEASE_DATE'" >> $@
	echo -e "sitemap:\n priority: 0.5" >> $@
	echo "---" >> $@
	echo "" >> $@

$(VERSION_EXAMPLE_TARGET)/%.md: $(EXAMPLES_DIR)/%.md RELEASE
	cp $< $@
	source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $@
	sourec RELEASE; sed -i "2ipublishdate: $$RELEASE_DATE" $@

$(VERSION_DOC_TARGET)/man-pass-team.md: $(VERSION_DOC_TEMPLATE)/man-pass-team.md RELEASE
	# cp and remove header (replaced by the template's header), some formatting
	# for the web
	source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $@
	sourec RELEASE; sed -i "2ipublishdate: $$RELEASE_DATE" $@
	sed -ne '/# NAME/,$$ p' $(MAN_SOURCES_MD) >> $@
	sed -i 's|pass(1)|[pass(1)]($(MAN_PAGE_PASS))|' $@
	sed -i 's|git(1)|[git(1)]($(MAN_PAGE_GIT))|' $@
	sed -i 's|gpg(1)|[gpg(1)]($(MAN_PAGE_GPG))|' $@

$(VERSION_DOC_TARGET)/installation.md: $(VERSION_DOC_TEMPLATE)/installation.md RELEASE
	cp $(VERSION_DOC_TEMPLATE)/installation.md $@
	source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $@
	sourec RELEASE; sed -i "2ipublishdate: $$RELEASE_DATE" $@
	# cp and remove header (replaced by the template's header)
	sed '0,/# Install/{/# Install/d}' $(DOCS_DIR)/installation_from_packages.md >> $@
	# replace references with hugo-compatible ones
	sed -i 's|(\.\/installation_from_sources.md|($(REPOSITORY_TREE)/$(DOCS_DIR)/installation_from_sources.md|' $@
	# replace installation links
	sed -i 's|\[here\](.*)|from the [download page]({{< ref "../downloads" >}})|g' $@

$(CONTENT_TARGET)/%.md: $(CONTENT_TEMPLATE)/%.md RELEASE
	cp $< $@
	# cp and remove first title (replaced by the template's header)
	@if [ ! -s "$@" ] ; then \
		echo "---" >> $@ ; \
		grep -m1 "^# " $(@F) | awk '{$$1="title:"; print $$0}' >> $@ ; \
		source RELEASE ; echo "publishdate: '$$RELEASE_DATE'" >> $@ ; \
		echo "lastmod: '$$RELEASE_DATE'" >> $@ ; \
		echo -e "sitemap:\n priority: 0.5" >> $@ ; \
		echo "---" >> $@ ; \
	else \
		source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $@ ; \
		sed -i "2ipublishdate: $$RELEASE_DATE" $@ ; \
	fi
	sed '0,/# /{/# /d}' $(@F) >> $@ ; \
	sed -i 's|(\.\/docs\/)|({{< ref "/$(VERSION)/docs/" >}})|' $@
	sed -i 's|(\.\/CREDITS.md|({{< ref "/CREDITS.md" >}}|' $@
	sed -i 's|(\.\/CODE_OF_CONDUCT.md|({{< ref "/CODE_OF_CONDUCT.md" >}}|' $@
	sed -i 's|(\.\/GPLv3.md|({{< ref "/GPLv3.md" >}}|' $@
	sed -i 's|(\.\/VISION_AND_GOALS.md|({{< ref "/VISION_AND_GOALS.md" >}}|' $@

.PRECIOUS: $(VERSION_EXAMPLES_TEMPLATE)/%.md
$(VERSION_EXAMPLES_TARGET)/%.md: $(VERSION_EXAMPLES_TEMPLATE)/%.md RELEASE
	cp $< $@
	source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $@
	sourec RELEASE; sed -i "2ipublishdate: $$RELEASE_DATE" $@
	# cp and remove first title (replaced by the template's header)
	sed '0,/# /{/# /d}' $(EXAMPLES_DIR)/$(@F) \
	| sed 's|\.\/\(.*\).sh|[\1.md]({{< ref "./\1.md" >}})|' >> $@
	sed -i 's|(\.\.\/\.\.\/$(MAN_SOURCES_MD)|({{< ref "../$(DOCS_DIR)/man-pass-team.md" >}}|' $@

$(VERSION_DOC_TARGET)/%.md: $(VERSION_DOC_TEMPLATE)/%.md RELEASE
	cp $< $@
	source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $@
	sourec RELEASE; sed -i "2ipublishdate: $$RELEASE_DATE" $@
	# cp and remove first title (replaced by the template's header)
	sed '0,/# /{/# /d}' $(DOCS_DIR)/$(@F) >> $@
	sed -i 's|(\.\/examples/|({{< ref "../examples/" >}}|' $@
	sed -i 's|(\.\.\/ROADMAP.md|({{< ref "/ROADMAP.md" >}}|' $@
	sed -i 's|(\.\.\/VISION_AND_GOALS.md|({{< ref "/VISION_AND_GOALS.md" >}}|' $@
	sed -i 's|(\.\.\/$(MAN_SOURCES_MD)|({{< ref "./man-pass-team.md" >}}|' $@
	sed -i 's|(\.\/concepts.md|({{< ref "./concepts.md" >}}|' $@


$(VERSION_DOC_TEMPLATE)/man-pass-team.md: $(MAN_SOURCES_MD) $(CONTENT_TARGET)
	touch $@

$(VERSION_DOC_TEMPLATE)/installation.md: $(DOCS_DIR)/installation_from_packages.md $(CONTENT_TARGET)
	touch $@

$(VERSION_DOC_TEMPLATE)/%.md: $(DOCS_DIR)/%.md $(CONTENT_TARGET)
	touch $@

$(VERSION_EXAMPLES_TEMPLATE)/%.md: $(EXAMPLES_DIR)/%.md $(CONTENT_TARGET)
	touch $@

$(VERSION_EXAMPLES_TEMPLATE): $(VERSION_EXAMPLES_TARGET)
	mkdir -p $@

$(VERSION_EXAMPLES_TARGET):
	mkdir -p $@

$(CONTENT_TEMPLATE)/%.md: %.md $(CONTENT_TARGET)
	touch $@

$(CONTENT_TARGET)/$(VERSION): $(CONTENT_TARGET) VERSION RELEASE
	mv $(CONTENT_TARGET)/__VERSION__ $(CONTENT_TARGET)/$(VERSION)
	for f in $$(grep -Lr "lastmod" $(CONTENT_TARGET)/$(VERSION)) ; do \
		source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $$f ; \
	done
	for f in $$(grep -Lr "publishdate" $(CONTENT_TARGET)/$(VERSION)) ; do \
		source RELEASE; sed -i "2ipublishdate: $$RELEASE_DATE" $$f ; \
	done

$(CONTENT_TARGET): $(CONTENT_TEMPLATE) $(CONTENT_ARCHIVE)
	rm -r $(CONTENT_TARGET) || true
	mkdir -p $(CONTENT_TARGET)
	if [ -d $(CONTENT_ARCHIVE) ] ; then \
		for version_dir in $$(find $(CONTENT_ARCHIVE) -mindepth 1 -maxdepth 1 -type d) ; do \
			cp -r -t $(CONTENT_TARGET) "$$version_dir" ; \
		done ; \
	fi
	cp -r -t $(CONTENT_TARGET) $(CONTENT_TEMPLATE)/*
	for f in $$(grep -L "lastmod" $(CONTENT_TARGET)/*md) ; do \
		source RELEASE; sed -i "2ilastmod: $$RELEASE_DATE" $$f ; \
	done
	for f in $$(grep -Lr "publishdate" $(CONTENT_TARGET)/*md) ; do \
		source RELEASE; sed -i "2ipublishdate: $$RELEASE_DATE" $$f ; \
	done

$(VERSION_DATA_TARGET)/downloads.yaml: $(VERSION_DATA_TARGET) $(VERSION_DATA_TEMPLATE)/downloads.yaml
	VARS=$$(printenv | sed -E 's/^([^=]+)=.*/$${\1}/') ; \
	    envsubst "$$VARS" < "$(VERSION_DATA_TEMPLATE)/downloads.yaml" > $@ ;

$(VERSION_DATA_TARGET): $(DATA_TARGET)
	mkdir -p $@

$(DATA_TARGET): $(DATA_ARCHIVE)
	rm -r $(DATA_TARGET) || true
	if [ -d $(DATA_ARCHIVE) ] ; then \
		cp -r $(DATA_ARCHIVE) $(DATA_TARGET) ; \
	fi

#######################################
# CLEAN
#######################################

clean: clean-build clean-pages clean-examples clean-playground clean-coverage

.PHONY: clean-coverage
clean-coverage:
	rm -r "$$(pwd)/$(DEFAULT_COVERAGE_OUTDIR)" || true

.PHONY: clean-examples
clean-examples:
	GLOBIGNORE="$(EXAMPLES_DIR)/README.md" ; rm $(EXAMPLES_DIR)/*md || true ; unset GLOBIGNORE

.PHONY: clean-pages
clean-pages:
	rm -r $(CONTENT_TARGET) || true
	rm -r $(DATA_TARGET) || true

.PHONY: clean-build
clean-build:
	rm -r $(BUILD_DIR) || true

.PHONY: clean-playground
clean-playground:
	rm -r $(PLAYGROUND_DIR) || true

.PHONY: clean-playground-keep-gpg
clean-playground-keep-gpg:
	rm $(PLAYGROUND_ENV) || true
	rm -r $(PLAYGROUND_PASSWORD_STORE_DIR) || true
